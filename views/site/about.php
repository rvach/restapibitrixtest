<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\helpers\Url;


$url = Url::to(['chat/index'], true);


echo "<a href='{$url}'>Перейти к чату</a>";

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
        https://rvachev.onrg.net/index.php?r=chat
    </p>

    <code><?= __FILE__ ?></code>
</div>
