<?php

use app\models\ChatHistory;
use yii\bootstrap5\ActiveForm;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\models\ChatHistory $model */
/** @var yii\widgets\ActiveForm $form */


?>
<div class="chat-history-index">

    <h1><?php echo "Привет, $userID!"; ?></h1>

    <div class="chat-history-form" id="chat_history_form">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'user_id')->hiddenInput(['value' => $userID])->label(false); ?>
        <?= $form->field($model, 'autor')->hiddenInput(['value' => $userName])->label(false); ?>
        <?= $form->field($model, 'message')->textInput(['maxlength' => true, 'value' => '']) ?>

        <div class="smiley-buttons">
            <button type="button" class="smiley-button" data-smiley="😊">😊</button>
            <button type="button" class="smiley-button" data-smiley="😄">😄</button>
            <button type="button" class="smiley-button" data-smiley="😍">😍</button>
            <button type="button" class="smiley-button" data-smiley="😎">😎</button>
            <button type="button" class="smiley-button" data-smiley="😇">😇</button>
            <button type="button" class="smiley-button" data-smiley="🥰">🥰</button>
            <button type="button" class="smiley-button" data-smiley="😜">😜</button>
            <button type="button" class="smiley-button" data-smiley="🤩">🤩</button>
            <button type="button" class="smiley-button" data-smiley="🙃">🙃</button>
            <button type="button" class="smiley-button" data-smiley="😴">😴</button>
            <button type="button" class="smiley-button" data-smiley="🤔">🤔</button>
            <button type="button" class="smiley-button" data-smiley="😷">😷</button>
            <button type="button" class="smiley-button" data-smiley="🤯">🤯</button>
            <button type="button" class="smiley-button" data-smiley="😭">😭</button>
            <button type="button" class="smiley-button" data-smiley="🤪">🤪</button>
        </div>

        <?= $form->field($model, 'date')->hiddenInput(['value' => time()])->label(false); ?>

        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <script>
        document.addEventListener('click', function (event) {
            if (event.target.classList.contains('smiley-button')) {
                const smiley = event.target.getAttribute('data-smiley');
                const messageInput = document.getElementById('chathistory-message');
                messageInput.value += smiley;
            }

        });
    </script>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//          ['class' => 'yii\grid\SerialColumn'],
'user_id',
            'autor',
            [
                'attribute' => 'message',
                'format' => 'html',
                'value' => function ($model) {
                    $emojiReplacements = [
                        '😊' => '<span>😊</span>',
                        '😄' => '<span>😄</span>',
                        '😍' => '<span>😍</span>',
                        '😎' => '<span>😎</span>',
                        '😇' => '<span>😇</span>',
                        '🥰' => '<span>🥰</span>',
                        '😜' => '<span>😜</span>',
                        '🤩' => '<span>🤩</span>',
                        '🙃' => '<span>🙃</span>',
                        '😴' => '<span>😴</span>',
                        '🤔' => '<span>🤔</span>',
                        '😷' => '<span>😷</span>',
                        '🤯' => '<span>🤯</span>',
                        '😭' => '<span>😭</span>',
                        '🤪' => '<span>🤪</span>',

                    ];
                    $messageWithEmoji = strtr($model->message, $emojiReplacements);
                    return $messageWithEmoji;
                },
            ],

            [
                'attribute' => 'date',
                'format' => 'html',
                'value' => function ($model) {
                    return date('H:i, j.n.Y', $model->date);
                }
            ],
            // Add delete functionality for the user's own entries
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model) use ($userID) {
                        if ($userID === $model->user_id) {
                            return Html::a('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
  <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
</svg>',
                                ['delete', 'id' => $model->id], [
                                'title' => 'Delete',
                                'data' => [
                                    'confirm' => 'Уверены что хотите удалить сообщение?',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                    },
                ],
            ],
        ],
    ]); ?>

</div>
