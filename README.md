<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Локальное приложение для Битрикс 24 на YII2</h1>
    <br>
</p>

Это тестовое задание, по созданию локального приложения для Битрикс 24. Соединение осуществляется через restApi.
Приложение представляет собой простой мессенджер, с базовым набором смайлов и возможностью удалять свои сообщения.
Данные для отображения текущего пользователя берутся из ответа на запрос в Битрикс 24. Приложение отображается внутри Битрикс.
