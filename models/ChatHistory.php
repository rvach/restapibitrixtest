<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat_history".
 *
 * @property int $id
 * @property int $user_id
 * @property string $autor
 * @property string $message
 * @property int $date
 */
class ChatHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'autor', 'message', 'date'], 'required'],
            [['user_id', 'date'], 'integer'],
            [['autor'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'autor' => 'Автор',
            'message' => 'Сообщение',
            'date' => 'Дата',
        ];
    }
}
