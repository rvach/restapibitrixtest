<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chat_history}}`.
 */
class m231006_081903_create_chat_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chat_history}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(10)->notNull(),
            'autor' => $this->string(50)->notNull(),
            'message' => $this->string(250)->notNull(),
            'date' => $this->integer(15)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%chat_history}}');
    }
}
