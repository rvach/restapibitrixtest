<?php

namespace app\controllers;

use app\models\ChatHistory;
use CRest;
use CRestCurrent;
use Yii;
use yii\caching\FileCache;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ChatController implements the CRUD actions for ChatHistory model.
 */
class ChatController extends Controller
{

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }


    public $layout = 'empty';

    public function beforeAction($action)
    {
        if (in_array($action->id, ['index'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays the chat index page and manages chat history.
     *
     * This action method is responsible for rendering the chat index page and interacting with the chat history.
     * It also retrieves data about the current user through the Bitrix API, including the user's name and ID,
     * and then inserts this data into the page, as well as saving it in the cache for later use.
     *
     * @return string
     */

    public function actionIndex()
    {   //Для теста работы формы
        /*$userData['result']['NAME'] = 'Мефодий Интроверт';
        $userData['result']['ID'] = 2;*/

        $cache = new FileCache();
        $userData = CRestCurrent::call('user.current');
        if (isset($userData['result']['NAME']) && isset($userData['result']['ID'])) {
            $cache->set('userName', $userData['result']['NAME']);
            $cache->set('userID', $userData['result']['ID']);
        }

        $userName = $cache->get('userName', 'Пользователь не найден');
        $userID = $cache->get('userID', null);

        $dataProvider = new ActiveDataProvider([
            'query' => ChatHistory::find(),
        ]);

        $model = new ChatHistory();
        $params = [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'userName' => $userName,
            'userID' => $userID,
        ];

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->render('index', $params);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('index', $params);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ChatHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ChatHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChatHistory::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
